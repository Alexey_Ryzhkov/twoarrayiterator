package com.company;

import java.lang.reflect.Array;
import java.util.*;

class ArrayIterator<T> implements Iterator<T> {

    private T[][] array;
    private T currentElement;
    private int i, j,j1;
    public ArrayIterator(T[][] array) {
        this.array = array;
    }
    @Override
    public boolean hasNext() {
        return i < array.length && j < array[i].length;
    }
    @Override
    public T next() {
        j1 = j;
        T result = array[i][j++];
        currentElement = result;
        if (j >= array[i].length) {
            i++;
            j = 0;
        }
        return result;
    }

    @Override
    public void remove() {
        ArrayList<T> temp = new ArrayList<>();
        for (T t : array[i]) {
            temp.add(t);
        }
        temp.remove(j1);
        T[] newArr = (T[]) temp.toArray();
        for (int i1 = 0; i1 < array[i].length-1; i1++) {
            T temp2 = newArr[i1];
            array[i][i1] = temp2;
        }
    }
}

public class Main {

    public static void main(String[] args) {
        Integer[][] array = new Integer[][]{
                {1,55,2,3},{3,4,5},{5,6,7}
        };
        ArrayIterator arrayIterator = new ArrayIterator(array);
        System.out.println("Пробежка по исходному массиву");
        arrayIterator.forEachRemaining(System.out::println);
        arrayIterator = new ArrayIterator(array);
        arrayIterator.next();
        arrayIterator.next();
        arrayIterator.remove();
        arrayIterator = new ArrayIterator(array);
        System.out.println("Массив после удаления второго элемента");
        arrayIterator.forEachRemaining(System.out::println);
    }
}
